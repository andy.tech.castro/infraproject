variable "project" {
    description = "El id del proyecto de Google Cloud"
    type = string
}

variable "region" {
    description = "Region para desplegar los recursos"
    type = string
}

variable "zone" {
    description = "Zona para desplegar los recursos"
    type = string
}

variable "nombrePI" {
    description = "Nombre asignado internamente al proyecto"
    type = string
}

variable "service_account" {
    description = "Service account to attach to the instance. See https://www.terraform.io/docs/providers/google/r/compute_instance_template.html#service_account."
    type = object({
        email  = string
        scopes = set(string)
    })
    default = {
        email = ""
        scopes = ["cloud-platform"]
    }
}

#Group

variable "min_replicas" {
    description = "Mínimo número de réplicas en el grupo"
}

variable "max_replicas" {
    description = "Número máximo de réplicas"
}

variable "autoscaling_enabled" {
    description = "Permitir el autoescalado"
}

variable "db_name" {
    description = "Nombre de base de datos"
}

variable "authorized_networks" {
  default = [{
    name  = "sample-gcp-health-checkers-range"
    value = "130.211.0.0/28"
  }]
  type        = list(map(string))
  description = "List of mapped public networks authorized to access to the instances. Default - short range of GCP health-checkers IPs"
}
