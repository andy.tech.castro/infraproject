project = "andres-prueba-terraform"
region = "europe-west1"
zone = "europe-west1-b"
nombrePI = "carta"
service_account = {
    email = ""
    scopes = ["cloud-platform"]
}

min_replicas = 1
max_replicas = 3
autoscaling_enabled = true

db_name = "carta-db"
