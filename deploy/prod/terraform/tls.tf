provider "acme" {
  server_url = "https://acme-v02.api.letsencrypt.org/directory"
}

resource "tls_private_key" "private_key_menu" {
  algorithm = "RSA"
}

resource "acme_registration" "reg" {
  account_key_pem = tls_private_key.private_key_menu.private_key_pem 
  email_address   = "andy.tech.castro@gmail.com"
}

resource "acme_certificate" "certificate" {
  account_key_pem           = acme_registration.reg.account_key_pem
  common_name               = "menuoneclick.andrescastro.es"
  subject_alternative_names = ["test.menuoneclick.andrescastro.es"]

  dns_challenge {
    provider = "gcloud"

    config = {
        GCE_PROJECT = var.project 
    }
  }
}
