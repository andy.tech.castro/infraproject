provider "google" {
    credentials = file("account.json")
    project = var.project
    region = var.region
    zone = var.zone
}

terraform {
    backend "gcs" {
        bucket = "state-terraform-andres"
        prefix = "network-tfstate"
    }
}

resource "google_compute_network" "network"{
    name = format("%s-network", var.nombrePI)
    auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "subnetwork" {
    name = format("%s-subnetwork-1", var.nombrePI)
    ip_cidr_range = "10.2.0.0/16"
    region = var.region
    network = google_compute_network.network.self_link
}

resource "google_compute_subnetwork" "subnetwork-2" {
    name = format("%s-subnetwork-2", var.nombrePI)
    ip_cidr_range = "10.3.0.0/16"
    region = var.region
    network = google_compute_network.network.self_link
}

resource "google_compute_router" "router-1" {
  name    = "lb-http-router"
  network = google_compute_network.network.self_link
  region  = var.region
}

resource "google_compute_firewall" "laravel-echo" {
  name    = "echo-firewall"
  network = google_compute_network.network.name

  source_ranges = [
    "0.0.0.0/0"
  ]

  allow {
    protocol = "tcp"
    ports    = ["6001", "22"]
  }

  source_tags = ["echo-server"]
}

module "cloud-nat" {
  source     = "terraform-google-modules/cloud-nat/google"
  version    = "1.0.0"
  router     = google_compute_router.router-1.name
  project_id = var.project
  region     = var.region
  name       = "cloud-nat-lb-http-router"
}

module "template" {
    source = "github.com/terraform-google-modules/terraform-google-vm/modules/instance_template"
    project_id = var.project
    subnetwork = google_compute_subnetwork.subnetwork.self_link 
    name_prefix = var.nombrePI
    tags = [
        google_compute_network.network.name,
        module.cloud-nat.router_name,
        "echo-server"
    ]
    service_account = var.service_account
    source_image_project = var.project
    source_image_family = "proyecto-final"
}

module "mig" {
    source              = "github.com/terraform-google-modules/terraform-google-vm/modules/mig"
    project_id          = var.project
    region              = var.region
    hostname            = google_compute_network.network.name
    subnetwork          = google_compute_subnetwork.subnetwork.self_link 
    autoscaling_enabled = var.autoscaling_enabled
    min_replicas        = var.min_replicas
    max_replicas        = var.max_replicas
    instance_template   = module.template.self_link
    named_ports = [{
        name = "http",
        port = 80
    },
    {
        name = "https",
        port = 443
    },
    {
        name = "node",
        port = 6001
    }]
}

module "gce-lb-http" {
  source            = "github.com/terraform-google-modules/terraform-google-lb-http"
  name              = "mig-http-lb"
  project           = var.project
  target_tags       = [google_compute_network.network.name]
  firewall_networks = [google_compute_network.network.name]
  url_map           = google_compute_url_map.ml-bkd-ml-mig-bckt-s-lb.self_link
  create_url_map    = false
  ssl               = true
  private_key       = acme_certificate.certificate.private_key_pem
  certificate       = acme_certificate.certificate.certificate_pem

  backends = {
    default = {
      description                     = null
      protocol                        = "HTTP"
      port                            = 80
      port_name                       = "http"
      timeout_sec                     = 10
      connection_draining_timeout_sec = null
      enable_cdn                      = false
      session_affinity                = null
      affinity_cookie_ttl_sec         = null

      health_check = {
        check_interval_sec  = null
        timeout_sec         = null
        healthy_threshold   = null
        unhealthy_threshold = null
        request_path        = "/auth/login"
        port                = 80
        host                = null
        logging             = null
      }

      log_config = {
        enable      = true
        sample_rate = 1.0
      }

      groups = [
        {
          group                        = module.mig.instance_group
          balancing_mode               = null
          capacity_scaler              = null
          description                  = null
          max_connections              = null
          max_connections_per_instance = null
          max_connections_per_endpoint = null
          max_rate                     = null
          max_rate_per_instance        = null
          max_rate_per_endpoint        = null
          max_utilization              = null
        }
      ]
     iap_config = {
        enable               = false
        oauth2_client_id     = ""
        oauth2_client_secret = ""
      }
    }
  }
}

resource "google_compute_url_map" "ml-bkd-ml-mig-bckt-s-lb" {
  // note that this is the name of the load balancer
  name            = "mig-http-lb"
  default_service = module.gce-lb-http.backend_services["default"].self_link

  host_rule {
    hosts        = ["*"]
    path_matcher = "allpaths"
  }

  path_matcher {
    name            = "allpaths"
    default_service = module.gce-lb-http.backend_services["default"].self_link

    path_rule {
      paths = [
        "/images",
        "/images/*"
      ]
      service = google_compute_backend_bucket.assets.self_link
    }
  }
}

resource "google_compute_backend_bucket" "assets" {
  name        = random_id.assets-bucket.hex
  description = "Contains static resources for example app"
  bucket_name = google_storage_bucket.assets.name
  enable_cdn  = true
}

resource "random_id" "assets-bucket" {
  prefix      = "terraform-static-content-"
  byte_length = 2
}

resource "google_storage_bucket" "assets" {
  name     = random_id.assets-bucket.hex
  location = "EU"

  // delete bucket and contents on destroy.
  force_destroy = true
}

resource "google_storage_bucket_object" "image_logo" {
  name         = "images/defaults/logo.png"
  source       = "./images/defaults/logo.png"
  bucket       = google_storage_bucket.assets.name
}

resource "google_storage_object_acl" "image-acl_logo" {
  bucket         = google_storage_bucket.assets.name
  object         = google_storage_bucket_object.image_logo.name
  predefined_acl = "publicRead"
}

resource "google_storage_bucket_object" "image_user" {
  name         = "images/defaults/user.jpeg"
  source        = "./images/defaults/user.jpeg"
  bucket       = google_storage_bucket.assets.name
}

resource "google_storage_object_acl" "image-acl-user" {
  bucket         = google_storage_bucket.assets.name
  object         = google_storage_bucket_object.image_user.name
  predefined_acl = "publicRead"
}

resource "google_storage_bucket_object" "image_spain" {
  name         = "images/defaults/spain.svg"
  source        = "./images/defaults/spain.svg"
  content_type = "image/svg+xml"
  bucket       = google_storage_bucket.assets.name
}

resource "google_storage_object_acl" "image-acl-spain" {
  bucket         = google_storage_bucket.assets.name
  object         = google_storage_bucket_object.image_spain.name
  predefined_acl = "publicRead"
}

resource "google_storage_bucket_object" "image_uk" {
  name         = "images/defaults/uk.svg"
  source        = "./images/defaults/uk.svg"
  content_type = "image/svg+xml"
  bucket       = google_storage_bucket.assets.name
}

resource "google_storage_object_acl" "image-acl-uk" {
  bucket         = google_storage_bucket.assets.name
  object         = google_storage_bucket_object.image_uk.name
  predefined_acl = "publicRead"
}

resource "google_dns_record_set" "menuoneclickandrescastroes" {
    name = "menuoneclick.andrescastro.es."
    managed_zone = "menuoneclick-andrescastro-es"
    type = "A"
    ttl = 300

    rrdatas = [
        module.gce-lb-http.external_ip
    ]
}

resource "google_storage_bucket_iam_member" "all_users_viewers" {
    bucket = google_storage_bucket.assets.name
    role   = "roles/storage.legacyObjectReader"
    member = "allUsers"
}

module "memorystore" {
  source         = "github.com/terraform-google-modules/terraform-google-memorystore"
  name           = format("%s-memorystore", var.nombrePI)
  project        = var.project
  memory_size_gb = "1"
  enable_apis    = "true"
  authorized_network = google_compute_network.network.name
}

resource "random_id" "suffix" {
  byte_length = 5
}

locals {
  instance_name = "${var.db_name}-${random_id.suffix.hex}"
  network_name  = "${var.db_name}-safer-${random_id.suffix.hex}"
}

module "private-service-access" {
  source = "github.com/andytechcastro/terraform-google-sql-db/modules/private_service_access"
  project_id  = var.project
  vpc_network = google_compute_network.network.name
}

module "safer-mysql-db" {
  source = "github.com/andytechcastro/terraform-google-sql-db/modules/safer_mysql"
  name       = local.instance_name
  project_id = var.project

  database_version = "MYSQL_5_7"
  region           = var.region
  zone             = "c"
  tier             = "db-n1-standard-1"

  additional_users = [
    {
      name     = "andres"
      password = "12345678"
      host     = "%"
    },
  ]
  vpc_network      =  google_compute_network.network.self_link


  // Optional: used to enforce ordering in the creation of resources.
  module_depends_on = [module.private-service-access.peering_completed]
}
