# TERRAFORM y GOOGLE CLOUD
La infraestructura de este repositorio ha sido creado para una aplicación mucho mas grande que la demo presentada, solo para motivos de demostración.

## Monitoring
No he usado grafana dado que al usar una infraestructura fuera del docker era mejor usar la herramienta de monitorización de Google Cloud. Dejo una imagen de un panel personalizado.

![](monitoring1.jpg) 

![](monitoring2.jpg) 

![](monitoring3.jpg) 

![](monitoring4.jpg) 

## Desarrollo
Para la maquina de desarrollo he usado docker-compose, creando un conjunto de servidores personalizados para la aplicación

## Produccion
Para producción, con terraform, se levanta un servidor escalable, una base de datos la cual se puede acceder solamente desde la red, un servidor redis, storage para imágenes con una CDN, un balanceador de cargai, un certificado, todo con sus redes y firewalls.

